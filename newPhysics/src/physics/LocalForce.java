package physics;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

public class LocalForce extends GlobalForce {

	public org.newdawn.slick.Color color = org.newdawn.slick.Color.black;
	public enum TYPE { ATTRACTOR, DISTRACTOR }
	
	public float strength = 1;
	private Circle circle;
	private TYPE type = TYPE.ATTRACTOR;
	
	private LocalForce() {
		super();
	}

	public LocalForce(Vector2f force) {
		super(force);
	}

	public LocalForce(Vector2f force, float radius, Vector2f position) {
		this.force = force;
		this.circle = new Circle(position.x, position.y, radius);
		this.circle.setCenterX(position.x);
		this.circle.setCenterY(position.y);
		if (this.type == TYPE.ATTRACTOR) {
			this.color = Color.red;
		} else {
			this.color = Color.blue;
		}
	}

	public LocalForce(Vector2f force, float radius, Vector2f position, TYPE type, float strength) {
		this.type = type;
		this.strength = strength;
		this.force = force;
		this.circle = new Circle(position.x, position.y, radius);
		this.circle.setCenterX(position.x);
		this.circle.setCenterY(position.y);
		if (this.type == TYPE.ATTRACTOR) {
			this.color = org.newdawn.slick.Color.red;
		} else {
			this.color = org.newdawn.slick.Color.blue;
		}
	}

	public LocalForce(Vector2f force, float radius, Vector2f position, float strength) {
		this.strength = strength;
		this.force = force;
		this.circle = new Circle(position.x, position.y, radius);
		this.circle.setCenterX(position.x);
		this.circle.setCenterY(position.y);
		if (this.type == TYPE.ATTRACTOR) {
			this.color = org.newdawn.slick.Color.red;
		} else {
			this.color = org.newdawn.slick.Color.blue;
		}
	}
	

	public Vector2f getStrength(Vector2f center) {
		if (this.circle.contains(center.x, center.y)) {
			float distance = new Vector2f(circle.getCenter()).sub(center).length();
			float factor = distance/(2*circle.radius);
			
			factor *= strength;
			
			Vector2f forceVec = new Vector2f(center).sub(this.getCenter());
			
			forceVec.x *= factor;
			forceVec.y *= factor;
			
			if (this.type == TYPE.ATTRACTOR) {
				forceVec.negate();
			}
			
			return forceVec;
		}
		return new Vector2f(0,0);
	}
	
	public Vector2f getCenter() {
		return new Vector2f(this.circle.getCenter());
	}
	
	public void draw(Graphics g) {
		g.setColor(new Color(color.r, color.g, color.b, (float) 0.4));
		g.draw(circle);
		g.setColor(new Color(color.r, color.g, color.b, (float) 0.1));
		g.fill(circle);
	}

}
