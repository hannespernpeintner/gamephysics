package physics;
import static physics.Helper.findSide;
import static physics.Helper.mul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.MannTriangulator;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Triangulator;
import org.newdawn.slick.geom.Vector2f;

import physics.Entity.TYPE;



public class World extends BasicGame {
	
	enum STATE {
		SIMULATING,
		INPUT,
		INPUTSTATIC,
		INPUTSPRING,
		INPUTPIN,
		PLACEGRAVFIELD,
		PLACEMOTOR,
		SLICING,
		SHOOT,
		STEPPING
	}

	public STATE state;
	public STATE lastState;
	public boolean debugViewEnabled = true;
	public boolean stepRequested = false;
	public boolean positionCorrectionEnabled = false;
	public boolean speculativeContactsEnabled = true;
	private GlobalForce gloForce = new GlobalForce(0, 9.81f);
	//private GlobalForce gloForce = new GlobalForce(0, 0);
	private List<LocalForce> locForces = new ArrayList<LocalForce>();
	private List<Entity> entities = new ArrayList<Entity>();
	private List<Spring> springs = new ArrayList<Spring>();
	private List<PinConstraint> pins = new ArrayList<PinConstraint>();
	private List<Motor> motors = new ArrayList<Motor>();
	private CollisionHandler handler = new CollisionHandler();
	private List<Vector2f> pointList = new ArrayList<Vector2f>();
	private List<Entity> entitiesForSpring = new ArrayList<Entity>();
	private float oldDt = 0;
	private Vector2f firstClick = new Vector2f(0,0);
	private int mouseWheel = 0;
	private Entity shootEntity;
	private Vector2f shootStart = new Vector2f(0,0);
	
	public World(String title) {
		super(title);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {

		state = STATE.SIMULATING;
		
		handler.g = arg0.getGraphics();
		
		for (int i = 0; i < 0; i++) {


			Polygon pol = new Polygon();
			pol.addPoint(50*i+20, 50*i+20);
			pol.addPoint(50*i+20, 50*i+80);
			pol.addPoint(50*i+30, 50*i+90);
			pol.addPoint(50*i+60, 50*i+60);
			pol.addPoint(50*i+30, 50*i+20);
			Entity en = new Entity(pol, 0.5f, 1);
			en.setPolygon(pol.transform(Transform.createRotateTransform(new Random().nextFloat()/10)));
			this.entities.add(en);
		}
		
		for (int i = 0; i < 0; i++) {

			Rectangle pol = new Rectangle(i*35, 10,30, 30);
			Entity en = new Entity(pol, 0.5f, 1);
			en.setPolygon(pol.transform(Transform.createRotateTransform(new Random().nextFloat()/10)));
			this.entities.add(en);
		}


		Polygon staticEnPoly = new Polygon();
		staticEnPoly.addPoint(20, 520);
		staticEnPoly.addPoint(50, 480);
		staticEnPoly.addPoint(100, 360);
		staticEnPoly.addPoint(150, 410);
		staticEnPoly.addPoint(200, 400);
		staticEnPoly.addPoint(250, 420);
		staticEnPoly.addPoint(300, 450);
		staticEnPoly.addPoint(400, 460);
		staticEnPoly.addPoint(500, 450);
		staticEnPoly.addPoint(600, 500);
		staticEnPoly.addPoint(900, 550);
		Entity staticEn = new Entity(staticEnPoly, Float.POSITIVE_INFINITY, 0, 0);
		this.entities.add(staticEn);
	
	}
	



	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		//System.out.println("update started");
		float dt = arg1/1000.0f;
		

		if (state == STATE.SIMULATING) {

			doSimulationSteps(dt);
			
		} else if (state == STATE.STEPPING) {
			if (stepRequested) {
				doSimulationSteps(dt);
				stepRequested = false;
			}
		} else if (state == STATE.SHOOT) {
			doSimulationSteps(dt);
		} else if (state == STATE.PLACEGRAVFIELD) {
			doSimulationSteps(dt);
		}
		
		cleanup(arg0.getScreenWidth(), arg0.getScreenHeight());
		
		oldDt = dt;
		//System.out.println("update for " + dt + " s finished");
		
	}
	
	private void doSimulationSteps(float dt){
		
		int steps = 1;
		
		for(int i = 0; i < steps; i++) {
			processMotors();
			moveObjects(dt/steps, true);
			handleCollisions(dt/steps);
			moveObjects(dt/steps, false);
			handleCollisions(dt/steps);
			moveObjects(dt/steps, false);
			handleCollisions(dt/steps);
			//progress unused time
			processSprings();
			processPins();
			
		}
	}

	private void processMotors() {
		for (Motor m: motors) {
			m.doStep();
		}
	}

	private void cleanup(int i, int j) {
		
		Rectangle viewport = new Rectangle(0, 0, i, j);
		
		for (int a = 0; a < entities.size(); a++) {
			Entity e = entities.get(a);
			if (!viewport.contains(e.getPolygon().getCenterX(), e.getPolygon().getCenterY())) {
				List<Spring> allSpringsFromE = getSpringsForEntity(e);
				for(int f = 0; f < allSpringsFromE.size(); f++) {
					springs.remove(allSpringsFromE.get(f));
				}
				entities.remove(e);
			}
		}
		
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (state == STATE.SIMULATING) {
			if (button == 0) {

				spawnMassiveBoxes(x, y);

			} else {
				for (int i = 0; i < clickCount; i++) {

					Rectangle pol = new Rectangle(x, y, 60, 60);
					Entity en = new Entity(pol, 2, 1);
					entities.add(en);
				}
			}
						
		} else if (state == STATE.INPUT || state== STATE.INPUTSTATIC) {
			pointList.add(new Vector2f(x, y));
		} else if (state == STATE.PLACEGRAVFIELD) {
			if (button == 1) {
				LocalForce lf = new LocalForce(new Vector2f(5,5), 70+mouseWheel/10, new Vector2f(x, y));
				lf.strength = 0.1f;
				locForces.add(lf);
			} else {
				LocalForce lf = new LocalForce(new Vector2f(5,5), 70+mouseWheel/10, new Vector2f(x, y), LocalForce.TYPE.DISTRACTOR, 1);
				lf.strength = 0.1f;
				locForces.add(lf);
			}
		} else if (state == STATE.PLACEMOTOR) {
			if (button == 0) {
				Entity ent = getNearestEntity(x, y);
				Motor mo = new Motor(ent, 10);
				motors.add(mo);
			}
			state = STATE.SIMULATING;
		} else if (state == STATE.INPUTPIN) {
			if (button == 0) {
				Entity ent = getNearestEntity(x, y);
				PinConstraint pin = new PinConstraint(ent);
				pins.add(pin);
			}
			state = STATE.SIMULATING;
		} else if (state == STATE.INPUTSPRING) {
			if (entitiesForSpring.size() == 1) {
				entitiesForSpring.add(getNearestEntityExlcude(x, y, entitiesForSpring));
				createSpringFromPoints();
			} else {
				entitiesForSpring.add(getNearestEntityExlcude(x, y, entitiesForSpring));
			}
		} else if (state == STATE.STEPPING) {
			stepRequested = true;
		} else if (state == STATE.SLICING) {
			if (pointList.size() == 0) {
				pointList.add(new Vector2f(x,y));
			} else if(pointList.size() == 1) {
				pointList.add(new Vector2f(x,y));
				lastState = state;
				createSliceFromPoints();
				state = STATE.STEPPING;
			}
		} else if (state == STATE.SHOOT) {

			shootStart = new Vector2f(x,y);
			Circle shootCircle = new Circle(shootStart.x, shootStart.y, (shootEntity.getMass()*10)+(mouseWheel/100));
			shootEntity = new Entity(shootCircle, 1*(1+(mouseWheel/30)), 1, 0.01f);
			if (button == 0) {
				shootEntity.type = TYPE.MUNITION;
			}
			
			shootEntity.setLinVelocity(new Vector2f(shootStart.x, shootStart.y).sub(new Vector2f(x, y)));
			shootCircle = null;
		}
	}
	
	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		
		if (state == STATE.SHOOT) {
			if(shootStart == null) {
				shootStart = new Vector2f(oldx, oldy);
			}
			Circle shootCircle = new Circle(newx, newy, (shootEntity.getMass()*10)+(mouseWheel/100));
			shootEntity = new Entity(shootCircle, 1*(1+(mouseWheel/100)), 1, 0.01f);
			shootEntity.setLinVelocity(new Vector2f(oldx, oldy).sub(new Vector2f(newx, newy)));
		}
	};
	
	@Override
	public void mouseReleased(int button, int x, int y) {
		if (state == STATE.SHOOT) {
			if (button == 0) {
				shootEntity.type = TYPE.MUNITION;
			}
			if(shootStart == null) {
				shootStart = new Vector2f(x, y);
			}
			shootEntity.setLinVelocity(mul(shootStart.sub(new Vector2f(x, y)), 0.1f));
			shootStart = null;
			entities.add(shootEntity);
		}
	};

	@Override
	public void mouseWheelMoved(int change) {
		mouseWheel += change;
	};
	
	private Entity getNearestEntityExlcude(int x, int y, List<Entity> entitiesForSpring) {
		
		Entity nearest = entities.get(0);
		int smallestDistance = (int) entities.get(0).getCenter().sub(new Vector2f(x, y)).length();
		
		for (Entity e: entities) {
			if (entitiesForSpring.contains(e)) {
				continue;
			}
			int temp = (int) e.getCenter().sub(new Vector2f(x, y)).length();
			if (temp < smallestDistance) {
				smallestDistance = temp;
				nearest = e;
			}
		}
		
		return nearest;
	}

	private Entity getNearestEntity(int x, int y) {
		
		Entity nearest = entities.get(0);
		int smallestDistance = (int) entities.get(0).getCenter().sub(new Vector2f(x, y)).length();
		
		for (Entity e: entities) {
			int temp = (int) e.getCenter().sub(new Vector2f(x, y)).length();
			if (temp < smallestDistance) {
				smallestDistance = temp;
				nearest = e;
			}
		}
		
		return nearest;
	}
	
	private Vector2f getNearestPointFrom(Vector2f in, List<Vector2f> points) {
		Vector2f nearest = points.get(0);
		float nearestDist = Float.POSITIVE_INFINITY;
		
		for (int i = 0; i < points.size(); i++) {
			Vector2f dist = in.copy().sub(points.get(i));
			float length = dist.length();
			if (length == 0) {
				continue;
			} else if (length < nearestDist) {
				nearestDist = length;
				nearest = points.get(i);
			}
		}
		
		return nearest;
	}

	@Override
	public void keyPressed(int key, char c) {
		if (c == 'i') {
			prepareStateChange();
			state = STATE.INPUT;
		} else if (c == 'j') {
			prepareStateChange();
			state = STATE.INPUTSTATIC;
		} else if (c == 'k') {
			prepareStateChange();
			state = STATE.INPUTSPRING;
		}  else if (c == 'l') {
			prepareStateChange();
			state = STATE.INPUTPIN;
		} else if (c == 's') {
			prepareStateChange();
			state = STATE.SIMULATING;
		} else if (c == 'c') {
			prepareStateChange();
			state = STATE.STEPPING;
		} else if (c == 'd') {
			debugViewEnabled = !debugViewEnabled;
		} else if (c == 'o') {
			prepareStateChange();
			state = STATE.SLICING;
		} else if (c == 'r') {
			entities.clear();
			springs.clear();
			locForces.clear();
			mouseWheel = 1;
		} else if (c == 'f') {
			prepareStateChange();
			state = STATE.SHOOT;
		} else if (c == 'p') {
			positionCorrectionEnabled = !positionCorrectionEnabled;
		} else if (c == '�') {
			speculativeContactsEnabled = !speculativeContactsEnabled;
		} else if (c == 'g') {
			if (gloForce.getForce().length() != 0) {
				gloForce.setForce(new Vector2f(0,0));
			} else {
				gloForce.setForce(new Vector2f(0.0f,9.81f));
			}
		} else if (c == 'h') {
			prepareStateChange();
			state = STATE.PLACEGRAVFIELD;			
		} else if (c == 'm') {
			prepareStateChange();
			state = STATE.PLACEMOTOR;			
		} else if (key == 205) {
			gloForce.setForce(new Vector2f(gloForce.getForce().x-2f, gloForce.getForce().y ));
		} else if (key == 203) {
			gloForce.setForce(new Vector2f(gloForce.getForce().x+2f, gloForce.getForce().y ));
		} else if (key == 200) {
			gloForce.setForce(new Vector2f(gloForce.getForce().x, gloForce.getForce().y+2f ));
		} else if (key == 208) {
			gloForce.setForce(new Vector2f(gloForce.getForce().x, gloForce.getForce().y-2f ));
		}
	};
	
	private void prepareStateChange(){
		lastState = state;
		createEntityFromPoints();
		createSpringFromPoints();
		createSliceFromPoints();
	}
	
	private void createSliceFromPoints() {
		if (pointList.isEmpty()) {
			return;
		} else if (pointList.size() == 2) {
			Line sliceLine = new Line(pointList.get(0).x, pointList.get(0).y, pointList.get(1).x, pointList.get(1).y );
			Entity sliceEntity = new Entity(sliceLine, 0, 0);
			
			if(lastState == STATE.SLICING) {
				Entity e = getNearestEntity((int) pointList.get(0).x, (int) pointList.get(0).y);
				if (e.getPolygon().intersects(sliceLine)) {
					
					List<Vector2f> right = new ArrayList<Vector2f>();
					List<Vector2f> left = new ArrayList<Vector2f>();
					
					List<Entity> temp = new ArrayList<Entity>();
					temp.add(sliceEntity);
					temp.add(e);
					List<Contact> contacts = handler.detectContacts(temp);
					
					{
						right.add(contacts.get(0).getHitResults().get(0).pt.copy());
						right.add(contacts.get(0).getHitResults().get(1).pt.copy());
						left.add(contacts.get(0).getHitResults().get(1).pt.copy());
						left.add(contacts.get(0).getHitResults().get(0).pt.copy());
					}
					
					for(int i = 0; i < e.getPolygon().getPointCount(); i++) {
						Vector2f point = new Vector2f(e.getPolygon().getPoint(i));
						int result = findSide(sliceLine.getX1(), sliceLine.getY1(), sliceLine.getX2(), sliceLine.getY2(), point.x, point.y);
						if (result == -1) {
							right.add(point.copy());
						} else if (result == 1) {
							left.add(point.copy());
						}
					}

					
					
					Polygon p1 = new Polygon();
					Polygon p2 = new Polygon();
					for (Vector2f point1: right) {
						p1.addPoint(point1.x, point1.y);
					}
					for (Vector2f point2: left) {
						
						p2.addPoint(point2.x, point2.y);
					}

					Entity e1 = new Entity(p1, e.getMass()/2, e.moi);
					Entity e2 = new Entity(p2, e.getMass()/2, e.moi);
					
					if (p1.closed()) {
						
						e1.setPolygon(p1);
						entities.add(e1);
					}
					if (p2.closed()) {
						
						e2.setPolygon(p2);
						entities.add(e2);
					}
					List<Spring> allSpringsFromE = getSpringsForEntity(e);
					for(int i = 0; i < allSpringsFromE.size(); i++) {
						springs.remove(allSpringsFromE.get(i));
					}
					entities.remove(e);
				}
			}
		}
		
		pointList.clear();
		
	}
	
	private List<Spring> getSpringsForEntity(Entity e) {
		List<Spring> temp = new ArrayList<Spring>();
		
		for (int i = 0; i < springs.size(); i++) {
			if (springs.get(i).getA().equals(e) || springs.get(i).getB().equals(e)) {
				temp.add(springs.get(i));
			}
		}
		
		return temp;
	}

	private void createSpringFromPoints() {
		if (entitiesForSpring.size() < 2) {
			entitiesForSpring.clear();
			return;
		} else if (entitiesForSpring.get(0).equals(entitiesForSpring.get(1))) {
			entitiesForSpring.clear();
			return;
		} else {
			springs.add(new Spring(this.entitiesForSpring.get(0), this.entitiesForSpring.get(1)));
			entitiesForSpring.clear();
		}
	}

	private void createEntityFromPoints() {
		
		if (pointList.isEmpty()) {
			return;
		}
		
		Polygon pol = new Polygon();
		for (Vector2f p: pointList) {
			pol.addPoint(p.x, p.y);
		}

		Entity en = new Entity(pol, 2, 1);
		if(lastState == STATE.INPUTSTATIC) {
			en.setMass(Float.POSITIVE_INFINITY);
			en.moi = 0;
		}
		entities.add(en);
		pointList.clear();
	}

	private void spawnMassiveCircles(float x, float y) {
		spawnMassiveCircles(new Vector2f(x,y));
	}
	private void spawnMassiveBoxes(float x, float y) {
		spawnMassiveBoxes(new Vector2f(x,y));
	}
	
	private void spawnMassiveCircles(Vector2f pos) {
		int amount = 8;
		int radius = 10 + mouseWheel/50;
		int rows = 3;
		int currentRow = 0;
		int gap = 20;
		
		for (int column = 0; column < (int)amount/rows; column++) {
			for (int row = 0; row < rows; row++) {
				float rand1 = new Random().nextFloat();
				float rand2 = new Random().nextFloat();
				Vector2f newPos = new Vector2f(pos.x + (column*(2*radius+(gap*rand1))), pos.y + (row*(2*radius+(gap*rand2))));
				Circle circle = new Circle(newPos.x, newPos.y, radius);
				Entity e = new Entity(circle, 2, 1);
				entities.add(e);
			}
		}
	}
	
	private void spawnMassiveBoxes(Vector2f pos) {
		int amount = 80;
		int radius = 10 + mouseWheel/50;
		int rows = 3;
		int currentRow = 0;
		int gap = 20;
		
		for (int column = 0; column < (int)amount/rows; column++) {
			for (int row = 0; row < rows; row++) {
				float rand1 = new Random().nextFloat();
				float rand2 = new Random().nextFloat();
				Vector2f newPos = new Vector2f(pos.x + (column*(radius+(gap*rand1))), pos.y + (row*(radius+(gap*rand2))));
				Rectangle rect = new Rectangle(newPos.x, newPos.y, radius, radius);
				Entity e = new Entity(rect, 2, 1);
				entities.add(e);
			}
		}
	}

	private void moveObjects(float dt, boolean useForces) {

		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			e.doStep(dt, this.gloForce, this.locForces, useForces);
		}
	
		long endTime = System.currentTimeMillis();

		long duration = endTime - startTime;
		//System.out.println("Moving took " + duration + " ms");
	}
	
	private void processSprings() {
		for (Spring spring: springs) {
			spring.process();
		}
	}
	
	private void processPins() {
		for (PinConstraint pin: pins) {
			pin.process();
		}
	}
	

	private void handleCollisions(float dt) {
		
		this.entities = handler.doCollisions(this.entities, dt, positionCorrectionEnabled, speculativeContactsEnabled);
		
	}
	
	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		g.translate(arg0.getWidth()/2, arg0.getHeight()/2);
		//g.scale(0.5f, 0.5f);
		g.scale(1, 1);
		g.translate(-arg0.getWidth()/2, -arg0.getHeight()/2);
		
		g.setBackground(Color.white);
		g.setColor(Color.black);

		for (LocalForce lf: locForces) {
			lf.draw(g);
		}

		for (Entity e: entities) {
			if (entitiesForSpring.contains(e)) {
				e.draw(g, Color.green, debugViewEnabled);
			} else {
				e.draw(g, debugViewEnabled);	
			}
		}
		
		for (Spring s: springs) {
			s.draw(g, debugViewEnabled);
		}
		
		handler.draw(g, debugViewEnabled);
		
		if (debugViewEnabled) {
			g.setColor(Color.black);
			int x = 900;
			int y = 60;
			int gap = 20;
			int counter = 1;
			g.drawString("i draw", x,y);
			g.drawString("j draw static", x,y+(counter++)*gap);
			g.drawString("k spring", x,y+(counter++)*gap);
			g.drawString("l pin", x,y+(counter++)*gap);
			g.drawString("h input gravity field", x,y+(counter++)*gap);
			g.drawString("m attach motor", x,y+(counter++)*gap);
			g.drawString("s Simulation mode", x-10,y+(counter++)*gap);
			g.drawString("o Slice mode", x-10,y+(counter++)*gap);
			g.drawString("f Shoot mode", x-10,y+(counter++)*gap);
			g.drawString("c Step mode", x-10,y+(counter++)*gap);
			g.drawString("d toggles Debugview", x,y+(counter++)*gap);
			g.drawString("r resets all", x,y+(counter++)*gap);
			g.drawString("g resets gravity", x,y+(counter++)*gap);
			g.drawString("p position correction - " + positionCorrectionEnabled, x,y+(counter++)*gap);
			//g.drawString("� speculative contacts - " + speculativeContactsEnabled, x,y+(counter++)*gap);
			g.drawString("Current Mode: " + state.toString(), x-10,y+10+(counter++)*gap);
			g.drawString("Mousewheel: " + String.valueOf(mouseWheel), x-10,y+10+(counter++)*gap);
			g.drawString("Gravity: " + String.valueOf(gloForce.getForce().toString()), x-10,y+10+(counter++)*gap);
			
		}
		
		if(shootEntity != null && shootStart != null) {
			g.setColor(new Color(Color.red.r, Color.red.g, Color.red.b, 0.1f+shootEntity.getLinVelocity().length()/100));
			g.fill(shootEntity.getPolygon());
			Line shootLine = new Line(shootEntity.getCenter().x, shootEntity.getCenter().y, shootStart.x, shootStart.y);
		}	
		
		for(Vector2f p: pointList) {
			g.draw(new Circle(p.x, p.y, 5));
		}
		
		if(state == STATE.SLICING && pointList.size() == 1) {
			Line sliceLine = new Line(pointList.get(0).x, pointList.get(0).y, arg0.getInput().getMouseX(), arg0.getInput().getMouseY());
			g.draw(sliceLine);
		}

	}
	
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new World("ssdsdfsdf"));
		app.setDisplayMode(1200, 800, false);
		//app.setVSync(true);
		//app.setAlwaysRender(true);
		//app.setSmoothDeltas(true);
		app.setShowFPS(true);
		app.setMinimumLogicUpdateInterval(22);
		app.setMaximumLogicUpdateInterval(22);
//		app.setMinimumLogicUpdateInterval(11);
//		app.setMaximumLogicUpdateInterval(11);
        app.start();
	}
}

