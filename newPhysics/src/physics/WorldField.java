package physics;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class WorldField {
	
	private int fieldSize = 10;
	private Vector2f index;
	public List<Entity> entities = new ArrayList<Entity>();
	public Rectangle rect;
	
	public WorldField(Vector2f index, int size) {
		this.index = index;
		this.rect = new Rectangle(index.x*size, index.y*size, size, size);
		
	}

}
