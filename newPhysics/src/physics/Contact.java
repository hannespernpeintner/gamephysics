package physics;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.geom.GeomUtil.HitResult;

public class Contact {
	private Entity a;
	private Entity b;
	private List<HitResult> hitResults = new ArrayList<HitResult>();
	public Line collisionNormal;
	public Vector2f collisionVector;
	public Vector2f response = new Vector2f(0,0);
	public Vector2f impulse = new Vector2f(0,0);
	public float penetrationDepth = 0;
	
	public Contact(Entity a, Entity b) {
		this.a = a;
		this.b = b;
		Vector2f collPoint = new Vector2f();
		collPoint.x = a.getPolygon().getCenterX() - b.getPolygon().getCenterX();
		collPoint.y = a.getPolygon().getCenterY() - b.getPolygon().getCenterY();
	}

	public Entity getA() {
		return a;
	}

	public void setA(Entity a) {
		this.a = a;
	}

	public Entity getB() {
		return b;
	}

	public void setB(Entity b) {
		this.b = b;
	}

	public List<HitResult> getHitResults() {
		return hitResults;
	}

	public void setHitResults(List<HitResult> hitResults) {
		this.hitResults = hitResults;
	}

}
