package physics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.GeomUtil;
import org.newdawn.slick.geom.GeomUtil.HitResult;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.MannTriangulator;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Triangulator;
import org.newdawn.slick.geom.Vector2f;

import physics.Entity.TYPE;
import static physics.Helper.*;

public class CollisionHandler {
	public Graphics g;
	public List<Entity> entities = new ArrayList<Entity>();
	public List<Contact> contacts = new ArrayList<Contact>();
	
	public List<Contact> detectContacts(List<Entity> entities) {
		contacts = new ArrayList<Contact>();
	
		try {
		
		for (int i = 0; i < entities.size(); i++) {
			for (int j = 0; j < entities.size(); j++) {
				if (i == j) {
					continue;
				} else {

					Entity a = entities.get(i);
					Entity b = entities.get(j);
					
					if (a.getPolygon().intersects(b.getPolygon())) {
					//if (new Circle(a.getPolygon().getCenterX(), a.getPolygon().getCenterY(), a.getPolygon().getBoundingCircleRadius()).intersects(new Circle(b.getPolygon().getCenterX(), b.getPolygon().getCenterY(), b.getPolygon().getBoundingCircleRadius()))) {

						Contact c = new Contact(a, b);							
						
						GeomUtil util = new GeomUtil();
						HitResult result = null;
						float[] points = a.getPolygon().getPoints();
						for (int p = 0; p < a.getPolygon().getPointCount(); p++) {
							Line line;
							if (p == a.getPolygon().getPointCount()-1){
								Vector2f pointA = new Vector2f(a.getPolygon().getPoint(p));
								Vector2f pointB = new Vector2f(a.getPolygon().getPoint(0));
								line = new Line(pointA, pointB);
							}  else {
								Vector2f pointA = new Vector2f(a.getPolygon().getPoint(p));
								Vector2f pointB = new Vector2f(a.getPolygon().getPoint(p+1));
								line = new Line(pointA, pointB);
							}
							
							HitResult tempResult = util.intersect(b.getPolygon(), line);
							if (tempResult != null) {
								
								c.getHitResults().add(tempResult);
				
							}
							
						}
						
						Vector2f normal = new Vector2f(c.getHitResults().get(0).line.getNormal(0));
						c.collisionNormal = new Line(c.getHitResults().get(0).pt, normal);
						c.collisionVector = new Vector2f(c.collisionNormal.getDX(), c.collisionNormal.getDY());
						
						if (c.getHitResults().size() > 0) {
							contacts.add(c);
						}
					}
					
				}
			}
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	contacts = removeEqualContats(contacts);
	
	return contacts;
}

	private List<Contact> removeEqualContats(List<Contact> contacts2) {
		
		for (int i = 0; i < contacts2.size()-1; i++) {
			for (int j = i+1; j < contacts.size(); j++) {

				if (contacts2.get(i).getHitResults().get(0).pt.x == contacts2.get(j).getHitResults().get(0).pt.x
					&& contacts2.get(i).getHitResults().get(0).pt.y == contacts2.get(j).getHitResults().get(0).pt.y) {
					contacts2.remove(j);
				}
			}
		}
		
	return contacts2;
}

	public List<Entity> doCollisions(List<Entity> entities, float dt, boolean positionCorrectionEnabled, boolean speculativeContactsEnabled) {
		this.entities = entities;
		this.contacts = new ArrayList<Contact>();
		
		try {
		
			for (int i = 0; i < entities.size(); i++) {
				for (int j = i+1; j < entities.size(); j++) {
					if (i == j) {
						continue;
					} else {

						Entity a = entities.get(i);
						Entity b = entities.get(j);
						
						if (a.getPolygon().intersects(b.getPolygon())) {
							
							if(a.getInverseMass() == 0 && b.getInverseMass() == 0) {
								continue;
							}
							
							//do a binary search to find the collision time
							float startInterval = 0.0f; //not intersecting
							float endInterval = dt; //intersecting
							for(int z=0; z<4; z++)
							{
								float searchTime = (endInterval - startInterval) / 2.0f;
								a.updateTo(searchTime); 
								b.updateTo(searchTime); 
								if(a.getPolygon().intersects(b.getPolygon()))
								{
									endInterval = searchTime;
									//System.out.println("Intersecting at " + Float.toString(searchTime));
									//System.out.println("Pos y " + Float.toString(a.getPolygon().getCenterY()) + " " + Float.toString(b.getPolygon().getCenterY()));
								}
								else
								{
									startInterval = searchTime;
									//System.out.println("Not Intersecting at " + Float.toString(searchTime));
								}
							}
							//endInterval += 5.0f;
							//System.out.println("Updating to " + Float.toString(endInterval));
							a.updateTo(endInterval);
							b.updateTo(endInterval);
							
							float unusedTime = dt - endInterval;
							//save unused time and use somewhere else
							
							//System.out.println(a.getPolygon().intersects(b.getPolygon()));
							
							//System.out.println("End y " + Float.toString(a.getPolygon().getCenterY()) + " " + Float.toString(b.getPolygon().getCenterY()));
							
							Contact c = new Contact(a, b);
							
							if (a.type == TYPE.MUNITION && !a.collided) {
								Triangulator tri = b.getPolygon().getTriangles();
								for(int triCount = 0; triCount < tri.getTriangleCount(); triCount++) {

									float rand = new Random().nextFloat();
									Polygon poly = new Polygon();
									for(int vertCount = 0; vertCount < 3; vertCount++) {
										poly.addPoint(tri.getTrianglePoint(triCount, vertCount)[0],tri.getTrianglePoint(triCount, vertCount)[1]);
									}
									Entity e = new Entity(poly, 2, 1, 1);
									e.setLinVelocity(mul(b.getLinVelocity(), 0.1f));
									entities.add(e);
									entities.remove(j);
									a.collided = true;
									continue;
								}
							} else if (a.type == TYPE.BREAKABLE && !a.collided) {
								Triangulator tri = a.getPolygon().getTriangles();
								for(int triCount = 0; triCount < tri.getTriangleCount(); triCount++) {
									float rand = new Random().nextFloat();
									
									Polygon poly = new Polygon();
									for(int vertCount = 0; vertCount < 3; vertCount++) {
										poly.addPoint(tri.getTrianglePoint(triCount, vertCount)[0],tri.getTrianglePoint(triCount, vertCount)[1]);
									}

									Entity e = new Entity(poly, 2, 1);
									e.setLinVelocity(mul(a.getLinVelocity(), 1));
									entities.add(e);
									a.collided = true;
									entities.remove(i);
									continue;
								}
								a.collided = true;
							}
							
							GeomUtil util = new GeomUtil();
							HitResult result = null;
							float[] points = a.getPolygon().getPoints();
							for (int p = 0; p < a.getPolygon().getPointCount(); p++) {
								Line line;
								if (p == a.getPolygon().getPointCount()-1){
									Vector2f pointA = new Vector2f(a.getPolygon().getPoint(p));
									Vector2f pointB = new Vector2f(a.getPolygon().getPoint(0));
									line = new Line(pointA, pointB);
								}  else {
									Vector2f pointA = new Vector2f(a.getPolygon().getPoint(p));
									Vector2f pointB = new Vector2f(a.getPolygon().getPoint(p+1));
									line = new Line(pointA, pointB);
								}
								
								HitResult tempResult = util.intersect(b.getPolygon(), line);
								if (tempResult != null) {
									
									c.getHitResults().add(tempResult);
					
								}
								
							}
							
							float penetrationDepth = new Vector2f(c.getA().getPolygon().getCenter()).sub(c.getHitResults().get(0).pt).length();
							penetrationDepth -= new Vector2f(c.getB().getPolygon().getCenter()).sub(c.getHitResults().get(0).pt).length();
							
							//Vector2f normal = new Vector2f(c.getHitResults().get(0).line.getNormal(0));
							Vector2f normal = c.getHitResults().get(0).pt.copy().sub(b.getCenter()).normalise();
							c.collisionNormal = new Line(c.getHitResults().get(0).pt, b.getCenter());
							c.collisionVector = normal;
							c.penetrationDepth = penetrationDepth;
							
							if (c.getHitResults().size() > 0) {
								contacts.add(c);
							}
							
							a.updateTo(startInterval);
							b.updateTo(startInterval);
						}
						
					}
				}
			}
			
			sumUpHitResults();
		
			//sumUpTwoContactsBetweenTheSamePolys();
			
			if(speculativeContactsEnabled) {
				speculateContacts(dt);
			}
			
			doCollisionForces(contacts, dt);
			
			if(positionCorrectionEnabled) {
				correctPositionsIfIntersects();	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return entities;
	}
	

	private void sumUpHitResults() {
		for (int i = 0; i < contacts.size(); i++) {
			Contact c = contacts.get(i);
			
			HitResult result = c.getHitResults().get(0);
			
			for (int j = 1; j < c.getHitResults().size(); j++) {
				HitResult hr = c.getHitResults().get(j);
				result.pt = new Vector2f(result.pt.x + hr.pt.x,result.pt.y + hr.pt.y);
				
			}
			
			result.pt = mul(result.pt, 1/c.getHitResults().size());
			c.getHitResults().clear();
			c.getHitResults().add(result);
		}
		
	}

	private void speculateContacts(float dt) {
		for (int i = 0; i < contacts.size(); i++) {
			Contact c = contacts.get(i);
			Entity a = c.getA();
			Entity b = c.getB();
			float p = c.penetrationDepth;
			Vector2f v = a.getCenter().sub(c.getHitResults().get(0).pt);
			v.normalise();
			Polygon neu = (Polygon) a.getPolygon().transform(Transform.createTranslateTransform(mul(v, p).x, mul(v,p).y));
			neu = (Polygon) (neu.transform(Transform.createRotateTransform((float)-0.001*a.getRotVelocity()*a.moi)));
			
		}
	}

	private void correctPositionsIfIntersects() {
		for (int i = 0; i < contacts.size(); i++) {
			Contact c = contacts.get(i);
			
			boolean intersects = true;
			int steps = 150;
			if ((c.getA().getInverseMass() == 0) || c.penetrationDepth < 2) {
				continue;
			}
			
			while(intersects && steps > 0) {
				//Polygon neu =(Polygon) (c.getA().getPolygon().transform(Transform.createTranslateTransform((float)0.0001*c.collisionNormal.getNormal(0)[0], (float)0.0001*c.collisionNormal.getNormal(0)[1])));
				Polygon neu = (Polygon) c.getA().getPolygon().transform(Transform.createTranslateTransform((float)0.0001*c.getA().getLinVelocity().copy().negate().x, (float)0.0001*c.getA().getLinVelocity().copy().negate().y));
				neu = (Polygon) (neu.transform(Transform.createRotateTransform((float)-0.001*c.getA().getRotVelocity()*c.getA().moi)));
				
				if (!(neu.intersects(c.getB().getPolygon()))) {
					intersects = false;
				} else {
					c.getA().setPolygon(neu);
				}
				
				//c.getA().setPolygon(c.getA().getPolygon().transform(Transform.createTranslateTransform((float)0.0001*c.collisionNormal.getNormal(0)[0], (float)0.0001*c.collisionNormal.getNormal(0)[1])));	
				
				steps--;
//				if (!(c.getA().getPolygon().intersects(c.getB().getPolygon()))) {
//					intersects = false;
//				}
			}
			System.out.println("Steps not used: " + steps);
		}
	}


	private void sumUpTwoContactsBetweenTheSamePolys() {
		contacts = removeEqualContats(contacts);
	}


	private void doCollisionForces(List<Contact> contacts, float dt) {

		for (Contact c: contacts) {
			//System.out.println("Collision @ " + c.getHitResults().get(0).pt.x + " | " + c.getHitResults().get(0).pt.y);

			Entity a = c.getA();
			Entity b = c.getB();
			
			Vector2f vA = a.getLinVelocity();
			Vector2f vB = b.getLinVelocity();

			float mA = a.getMass();
			float mB = b.getMass();
			Vector2f relVelo = vA.copy().sub(vB.copy());


			float elasticy = 0.3f;
//			elasticy = 1-(a.cor - b.cor);
//			if (elasticy < 0) {
//				elasticy = -elasticy;
//			}
			
			//Vector2f normal = new Vector2f(c.getHitResults().get(0).line.getNormal(0));
			Vector2f normal = c.collisionVector;

			
//			if(dot(vB.copy().sub(vA.copy()), normal) < 0) {
//				String s = "";
//				return;
//			}
			
			// LINEAR
			

			Vector2f temp = mul(normal.copy(), (1+elasticy)* (dot(vA,normal.copy())));
			//Vector2f temp = mul(normal.copy(), (1+elasticy)* (dot(relVelo.copy(),normal.copy())));

			Vector2f I = mul(normal.copy(),(1+elasticy));
			I = mul(I.copy(), dot(relVelo.copy(), normal.copy()));
			float massPart = a.getInverseMass()+b.getInverseMass();
			I = mul(I.copy(), 1/massPart);
			

			a.subLinVelocity(mul(I.copy(), a.getInverseMass()));
			b.addLinVelocity(mul(I.copy(), b.getInverseMass()));
			//a.addLinVelocity(mul(vA.sub(temp).copy(), a.getInverseMass()*dt));
			c.response = vA.copy().sub(temp);
			c.impulse = I.copy();

			// ROTATION
			
			float cross = cross(vA.copy().negate(), normal.copy());
			a.setRotVelocity(cross);
			
			cross = cross(vB.copy().negate(), normal.copy().negate());
			b.setRotVelocity(cross);

		}
	}
	
	public void draw(Graphics g, boolean debugViewEnabled) {
		
		int bodyCount = entities.size();
		
		g.setColor(Color.black);
		if (debugViewEnabled){
			g.drawString("Total Bodies: " + String.valueOf(bodyCount), 50, 50);
			g.drawString("Total Contacts: " + String.valueOf(contacts.size()), 50, 70);

			for (Contact c: contacts) {
				
				//for (HitResult r: c.getHitResults()) {
				{HitResult r = c.getHitResults().get(0);
				
				
					g.setColor(Color.green);
					g.draw(r.line);
					
						
						Line collNormal = c.collisionNormal;
						try {
							//g.draw(collNormal);
							g.drawLine(r.pt.x, r.pt.y, r.pt.x-c.getA().getLinVelocity().x, r.pt.y-c.getA().getLinVelocity().y);
							//g.drawLine(r.pt.x, r.pt.y, r.pt.x+c.response.x, r.pt.y+c.response.y);

							g.setColor(Color.blue);
							g.drawLine(r.pt.x, r.pt.y, r.pt.x-c.impulse.x, r.pt.y-c.impulse.y);
							g.drawLine(r.pt.x, r.pt.y, r.pt.x+c.impulse.x, r.pt.y+c.impulse.y);
							g.drawString(String.valueOf(c.penetrationDepth), r.pt.x, r.pt.y);
							//g.drawLine(r.pt.x, r.pt.y, r.pt.x+c.response.x, r.pt.y+c.response.y);
						} catch(Exception e) {
							e.printStackTrace();
						}
	
					Circle ptCircle = new Circle(r.pt.getX(),r.pt.getY(), 5);
					g.setColor(Color.red);
					g.fill(ptCircle);
				}
				
			}
		}
	}

}
