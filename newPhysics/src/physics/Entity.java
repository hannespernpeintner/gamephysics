package physics;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;
import static physics.Helper.*;

public class Entity {
	
	public enum TYPE {
		REGULAR,
		BREAKABLE,
		MUNITION
	}
	
	public TYPE type = TYPE.REGULAR;
	public boolean collided = false;
	public Color color = new Color(Color.blue.r,Color.blue.r,Color.blue.r,(float)0.5) ;
	private Shape polygon;
	private Shape orgPolygon;
	public Vector2f oldPos;
	public float oldRot;
	public Vector2f motionVec = new Vector2f(0,0);
	private Vector2f linAcc = new Vector2f();
	private Vector2f linVelocity = new Vector2f();
	private Vector2f pos = new Vector2f();
	private float rot = 0.0f;
	private float rotAcc = 0.0f;
	private float rotVelocity = 0;
	private float mass = 10f;
	private float im = 0;
	public float moi = 1.0f;
	public float cor = 1f;

	public Entity (Shape polygon, float mass, float moi) {
		this(TYPE.REGULAR, polygon, mass, moi, 1f);
	}
	
	public Entity (Shape polygon, float mass, float moi, float cor) {
		this(TYPE.REGULAR, polygon, mass, moi, cor);
	}
	
	public Entity (TYPE type, Shape polygon, float mass, float moi, float cor) {
		this.pos = new Vector2f(polygon.getCenter());
		this.type = type;
		this.polygon = polygon.transform(Transform.createTranslateTransform(-pos.x, -pos.y));
		this.orgPolygon = this.polygon;
		this.mass = mass;
		this.moi = moi;
		this.cor = cor;
		this.im = 1f/mass;
		this.oldPos = new Vector2f(polygon.getCenter());
	}
	public Shape getPolygon() {
		return polygon;
	}
	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}
	public void setPolygon(Shape polygon) {
		this.polygon = (Shape) polygon;
	}
	public float getMass() {
		return mass;
	}
	public void setMass(Integer mass) {
		this.mass = (float) mass;
	}
	
	public void doStep(float dt, GlobalForce gloForce, List<LocalForce> locForces, boolean useForces) {
		
		oldPos = pos.copy();
		oldRot = rot;
		
		if(useForces)
		{
			float rotDamp = 0.98f;
			this.rotVelocity *= rotDamp;	
			
			float im = getInverseMass();
			Vector2f force = /*getLinAcc().copy().negate().add*/(gloForce.force);
			
			for (LocalForce f: locForces) {
				Vector2f strength = f.getStrength(new Vector2f(this.polygon.getCenter()).copy());
				//this.addRotAcc(dot(f.getCenter().copy(), this.getCenter().copy()));
				force.add(strength.copy());
			}
		
			if(im > 0)
				linVelocity = linVelocity.copy().add(force);
			//linVelocity = linVelocity.copy().add(mul(linAcc.copy(), getInverseMass()*dt));
			rotVelocity += rotAcc*moi;
		}
		
		rot += dt * rotVelocity;
		pos.add(linVelocity.copy().scale(dt));
		//System.out.println("oldPos " + oldPos + " new pos " + pos);
		

		
		this.setPolygon((Polygon) this.orgPolygon.transform(Transform.createTranslateTransform(pos.x, pos.y)));
		this.setPolygon((Polygon) this.polygon.transform(Transform.createRotateTransform(rot / 180.0f * (float)Math.PI, pos.x, pos.y)));
		
		// Rotation: (float)(2.0*Math.PI) ist komplette Drehung
		
		
		
		motionVec = new Vector2f(new Vector2f(polygon.getCenter())).add(linVelocity.copy());

	}
	
	public void updateTo(float searchTime)
	{
		rot = oldRot + searchTime * rotVelocity;
		pos = oldPos.copy().add(linVelocity.copy().scale(searchTime));
		//System.out.println("oldPos " + oldPos + " new pos " + pos);
		
		this.setPolygon((Polygon) this.orgPolygon.transform(Transform.createTranslateTransform(pos.x, pos.y)));
		this.setPolygon((Polygon) this.polygon.transform(Transform.createRotateTransform(rot / 180.0f * (float)Math.PI, pos.x, pos.y)));		
	}


	public Vector2f getLinAcc() {
		return linAcc;
	}
	public void setLinAcc(Vector2f linAcc) {
		this.linAcc = linAcc;
	}
	public Vector2f getLinVelocity() {
		return linVelocity;
	}
	public void setLinVelocity(Vector2f linVelocity) {
		this.linVelocity = linVelocity;
	}
	public float getRotAcc() {
		return rotAcc;
	}
	public void setRotAcc(float rotAcc) {
		this.rotAcc = rotAcc;
	}
	public float getRotVelocity() {
		return rotVelocity;
	}
	public void setRotVelocity(float rotVelocity) {
		this.rotVelocity = rotVelocity;
	}
	public void setMass(float mass) {
		this.mass = mass;
		this.im = 1.0f / mass;
	}
	public void addLinAcc(Vector2f acc) {
		this.linAcc.add(acc);
	}

	public void addLinVelocity(Vector2f acc) {
		this.linVelocity.add(acc);
	}
	public void subLinVelocity(Vector2f acc) {
		this.linVelocity.sub(acc);
	}

	public void addRotAcc(float f) {
		this.rotAcc += f;
	}
	
	public void addRotVelocity(float f) {
		this.rotVelocity += f;
	}
	public Vector2f getCenter() {
		return new Vector2f(this.polygon.getCenter());
	}
	
	public void draw(Graphics g, boolean debugViewEnabled) {
		g.setColor(color);
		g.fill(this.polygon);
		g.setColor(Color.black);
		g.draw(this.polygon);
		if (debugViewEnabled) {
			g.drawString(this.getLinVelocity().x + " - " + this.getLinVelocity().y , this.polygon.getCenterX(), this.polygon.getCenterY());
			g.drawString(String.valueOf(this.getRotVelocity()), this.polygon.getCenterX(), this.polygon.getCenterY()-15);
			
			g.setColor(Color.magenta);
			g.drawLine(polygon.getCenterX(), polygon.getCenterY(), motionVec.x, motionVec.y);
		}
		
	}
	
	public void draw(Graphics g, Color green, boolean debugViewEnabled) {
		g.setColor(green);
		g.fill(this.polygon);
		g.setColor(Color.black);
		g.draw(this.polygon);
		if (debugViewEnabled) {
//			g.drawString(this.getLinVelocity().x + " - " + this.getLinVelocity().y , this.polygon.getCenterX(), this.polygon.getCenterY());
//			g.drawString(String.valueOf(this.getRotVelocity()), this.polygon.getCenterX(), this.polygon.getCenterY()-15);
			
			g.setColor(Color.magenta);
			g.drawLine(polygon.getCenterX(), polygon.getCenterY(), motionVec.x, motionVec.y);
		}
	}
	
	public void subLinAcc(Vector2f v) {
		this.linAcc = this.linAcc.sub(v);
		
	}
	public float getInverseMass() {
		//return 1f/mass;
		return im;
	}


}
