package physics;

import static physics.Helper.mul;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;

public class Spring {
	private int targetWidth;
	private int currentWidth;
	private Entity a;
	private Entity b;
	private int maxStepsInit = 10;
	private int currentStep = 0;
	private float factor = 2f;
	private int tolerance;

	public Spring(Entity a, Entity b) {
		this(a, b, 12);
	}
	
	public Spring(Entity a, Entity b, int tolerance) {
		this.setA(a);
		this.setB(b);
		this.tolerance = tolerance;
		this.targetWidth = (int) getA().getCenter().sub(getB().getCenter()).length();
	}
	
	public void process() {
		Vector2f direction = getA().getCenter().sub(getB().getCenter()).normalise();
		currentWidth = (int) getA().getCenter().sub(getB().getCenter()).length();
		
		if (currentWidth >= targetWidth+tolerance) {
			while (currentWidth > targetWidth+tolerance && currentStep < maxStepsInit) {
				direction = mul(direction, factor*currentWidth/targetWidth);
				getA().addLinVelocity(direction.copy().negate());
				getB().addLinVelocity(direction);
				getA().setPolygon(getA().getPolygon().transform(Transform.createTranslateTransform(-direction.x*getA().getInverseMass(), -direction.y*getA().getInverseMass())));
				getB().setPolygon(getB().getPolygon().transform(Transform.createTranslateTransform(direction.x*getB().getInverseMass(), direction.y*getB().getInverseMass())));
				currentWidth = (int) getA().getCenter().sub(getB().getCenter()).length();
				currentStep++;
				
			} 
		} else if (currentWidth <= targetWidth-tolerance) {
			while (currentWidth <= targetWidth-tolerance && currentStep < maxStepsInit) {
				direction = mul(direction.negate(), factor*currentWidth/targetWidth);
				getA().addLinVelocity(direction);
				getB().addLinVelocity(direction.copy().negate());
				getA().setPolygon(getA().getPolygon().transform(Transform.createTranslateTransform(direction.x*getA().getInverseMass(), direction.y*getA().getInverseMass())));
				getB().setPolygon(getB().getPolygon().transform(Transform.createTranslateTransform(-direction.x*getB().getInverseMass(), -direction.y*getB().getInverseMass())));	
				currentWidth = (int) getA().getCenter().sub(getB().getCenter()).length();
				currentStep++;
			} 
		}
		//System.out.println(currentStep);
		currentStep = 0;
	}

	public int getWidth() {
		return targetWidth;
	}

	public void setWidth(int width) {
		this.targetWidth = width;
	}

	public Entity getA() {
		return a;
	}

	public void setA(Entity a) {
		this.a = a;
	}

	public Entity getB() {
		return b;
	}

	public void setB(Entity b) {
		this.b = b;
	}

	public int getTolerance() {
		return tolerance;
	}

	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}

	public void draw(Graphics g, boolean debugViewEnabled) {
		g.draw(new Line(getA().getCenter().x, getA().getCenter().y, getB().getCenter().x, getB().getCenter().y));
		g.drawGradientLine(getA().getCenter().x, getA().getCenter().y, getA().color, getB().getCenter().x, getB().getCenter().y, getB().color);
		//g.drawString(String.valueOf(currentWidth) + " / " + String.valueOf(targetWidth), getB().getCenter().x, getB().getCenter().y);
	}

}
