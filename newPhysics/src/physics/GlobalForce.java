package physics;

import org.newdawn.slick.geom.Vector2f;

public class GlobalForce {
	
	protected Vector2f force;
	
	public GlobalForce() {
		
	}

	public GlobalForce(Vector2f force) {
		this.force = force;
	}
	
	public GlobalForce(float x, float y) {
		this.force = new Vector2f(x, y);
	}

	public Vector2f getForce() {
		return force;
	}

	public void setForce(Vector2f force) {
		this.force = force;
	}

}
